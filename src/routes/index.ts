import homeRouter from './home'
import channelRouter from './channel'
import * as Koa from 'koa'

export const applyRoutes = (app: Koa) => {
    app.use(homeRouter.routes()).use(homeRouter.allowedMethods())
    app.use(channelRouter.routes()).use(channelRouter.allowedMethods())
}
