import { Context, DefaultState } from 'koa'
import * as Router from 'koa-router'
import { defaultRenderOptions } from '../logic/settings'
import { viewModelForChannelAdd } from '../logic/views'
import * as Managers from '../logic/data/managers'

const router = new Router<DefaultState, Context>({
    prefix: '/channel',
})

router.get('/add', async (ctx: Context, next) => {
    let channelAddVM = viewModelForChannelAdd()
    await ctx.render('channel-add', await defaultRenderOptions(channelAddVM))
})

router.post('/add', async (ctx: Context, next) => {
    let channelId = ctx.request.body.channel_id
    await Managers.channel.add(channelId)
    ctx.redirect('/')
})

export default router
