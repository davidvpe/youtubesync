import { Context, DefaultState } from 'koa'
import * as Router from 'koa-router'
import { DataManager } from '../logic/data'
import * as Managers from '../logic/data/managers'
import { defaultRenderOptions } from '../logic/settings'
import { viewModelForHome } from '../logic/views'

const router = new Router<DefaultState, Context>()

router.get('/', async (ctx: Context, next) => {
    let action = ctx.query['action'] as string
    let channelId = ctx.query['channel'] as string
    if (action && channelId) {
        if (action === 'refresh') {
            await Managers.channel.update(channelId)
        } else if (action === 'delete') {
            await Managers.channel.remove(channelId)
        }

        delete ctx.query['action']
        delete ctx.query['channel']
        let query = Object.keys(ctx.query)
            .map((k) => `${k}=${ctx.query[k]}`)
            .join('&')

        ctx.redirect('/' + (query.length > 0 ? `?${query}` : ''))
    } else {
        let channels = await Managers.channel.getTop()
        let viewModel = viewModelForHome(channels)
        await ctx.render('home', await defaultRenderOptions(viewModel))
    }
})
// router.get('/settings', async (ctx: Context, next) => {
//     let opts = {}

//     let mysqlCreds = await DataManager.getInstance().getMySQLCreds()
//     if (!mysqlCreds) {
//         opts = {
//             secureKey: DataManager.getInstance().isUnlocked(),
//             inputs: viewModelForSettings(),
//         }
//     } else if (mysqlCreds instanceof Error) {
//         opts = {
//             secureKey: false,
//             errors: [
//                 {
//                     short:
//                         'There was a problem decrypting the mysql credentials',
//                 },
//             ],
//         }
//     } else {
//         opts = {
//             secureKey: true,
//             inputs: viewModelForSettings(mysqlCreds),
//         }
//     }
//     await ctx.render('settings', opts)
// })
// router.post('/settings', async (ctx: Context, next) => {
//     let host = ctx.request.body.host
//     let port = ctx.request.body.port
//     let user = ctx.request.body.user
//     let pass = ctx.request.body.pass
//     let dbName = ctx.request.body.db_name
//     DataManager.getInstance().saveMySQLCreds({
//         host: host,
//         port: port,
//         user: user,
//         pass: pass,
//         dbName: dbName,
//     })
//     ctx.redirect('/')
// })

// router.post('/settings/secure', async (ctx: Context, next) => {
//     let secureKey = ctx.request.body.secure_key
//     await DataManager.getInstance().setSecureKey(secureKey)
//     ctx.redirect('/settings')
// })

export default router
