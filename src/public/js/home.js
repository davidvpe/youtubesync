$('.refresh').on('click', (e) => {
    let channelId = e.currentTarget.attributes.channel.value
    setButtonLoading(e.currentTarget)
    addParam('refresh', channelId)
})
$('.delete').on('click', (e) => {
    console.log(e.currentTarget)
    setButtonLoading(e.currentTarget)
    let channelId = e.currentTarget.attributes.channel.value
    addParam('delete', channelId)
})

function addParam(param, channel) {
    if (document.location.href.includes('?')) {
        var url = document.location.href + `&action=${param}&channel=${channel}`
    } else {
        var url = document.location.href + `?action=${param}&channel=${channel}`
    }
    document.location = url
}

const ioClient = io.connect()

ioClient.emit('home')

ioClient.on('home', (e) => {
    switch (e.event) {
        case 'videoDownloaded': {
            const channelDbId = e.channel
            const channelProgressBarDiv = $(
                `.progress-bar[channel='${channelDbId}']`
            )
            const totalDownloaded =
                parseInt(channelProgressBarDiv.attr('downloaded')) + 1
            channelProgressBarDiv.attr('downloaded', totalDownloaded)
            const total = channelProgressBarDiv.attr('total')
            console.log('total: ', total)
            channelProgressBarDiv.css(
                'width',
                `${(totalDownloaded * 100) / total}%`
            )
            channelProgressBarDiv.text(`${totalDownloaded} / ${total}`)
        }
    }
})
