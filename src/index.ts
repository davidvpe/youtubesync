import * as Koa from 'koa'
import * as HttpStatus from 'http-status-codes'
import * as views from 'koa-views'
import * as serve from 'koa-static'
import * as bodyParser from 'koa-bodyparser'
import { applyRoutes } from './routes'
import * as Dotenv from 'dotenv'
import { DataManager } from './logic/data'
import * as HTTP from 'http'
import * as SocketIO from 'socket.io'
import SocketManager from './logic/sockets'

Dotenv.config()

DataManager.getInstance()
    .loadFromEnv()
    .then(() => {
        startServer()
    })

const startServer = () => {
    const app: Koa = new Koa()

    const server = HTTP.createServer(app.callback())

    app.use(serve(`${__dirname}/public`))

    app.use(bodyParser())

    // Generic error handling middleware.
    app.use(async (ctx: Koa.Context, next: () => Promise<any>) => {
        try {
            await next()
        } catch (error) {
            ctx.status =
                error.statusCode ||
                error.status ||
                HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR
            error.status = ctx.status
            ctx.body = { error }
            ctx.app.emit('error', error, ctx)
        }
    })

    const render = views(`${__dirname}/views`, {
        extension: 'ejs',
        map: {
            hbs: 'ejs',
        },
        options: {},
    })

    // Must be used before any router is used
    app.use(render)
    // OR Expand by app.context
    // No order restrictions
    // app.context.render = render()

    applyRoutes(app)
    // Application error logging.
    app.on('error', console.error)

    const port = process.env.PORT || 3000

    const socket = new SocketIO.Server(server)

    SocketManager.getInstance().load(socket)

    server.listen(port, () => {
        console.log('Listening on port: ', port)
    })
}
