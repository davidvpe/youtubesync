import SocketManager from '.'
import { Channel, Video } from '../data/models'

interface ChannelUpdate {}

async function reportDownloadUpdate(channelDbId: number) {
    SocketManager.getInstance().sendToHome({
        channel: channelDbId,
        event: 'videoDownloaded',
    })
}

export { reportDownloadUpdate }
