import * as SocketIO from 'socket.io'

class SocketManager {
    private static instance: SocketManager
    private socket: SocketIO.Server
    private constructor() {}

    private homeSockets: SocketIO.Socket[] = []

    public static getInstance(): SocketManager {
        if (!SocketManager.instance) {
            SocketManager.instance = new SocketManager()
        }

        return SocketManager.instance
    }
    load(socket: SocketIO.Server) {
        this.socket = socket

        this.socket.on('connection', (socketConnected) => {
            socketConnected.on('home', () => {
                this.homeSockets.push(socketConnected)
                this.homeSockets.forEach((s) => {
                    s.emit('home', 'wohoooo')
                })
            })

            socketConnected.on('disconnect', () => {
                this.homeSockets.splice(
                    this.homeSockets.indexOf(socketConnected),
                    1
                )
            })
        })
    }

    sendToHome(message: any) {
        this.homeSockets.forEach((h) => h.emit('home', message))
    }
}

export default SocketManager
