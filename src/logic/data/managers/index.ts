import { DataManager } from '..'
import * as YT from '../youtube'

import * as ChannelManager from './channel'
import * as VideoManager from './video'

async function addChannel(channelId: string) {
    let channel = await ChannelManager.add(channelId)
    VideoManager.getChannelVideos(channel)
        .then((res: YT.ChannelVideosResponse) => {
            channel.last_fetched_page = res.lastPage
            channel.save()
            return res.videos
        })
        .then((videos: YT.VideoResponse[]) => {
            VideoManager.addMultiple(videos, channel.id)
        })
}

async function updateChannel(channelDbId: string) {
    let channel = await ChannelManager.update(channelDbId)
    VideoManager.getChannelVideos(channel)
        .then((res: YT.ChannelVideosResponse) => {
            channel.last_fetched_page = res.lastPage
            channel.save()
            return res.videos
        })
        .then((videos: YT.VideoResponse[]) => {
            VideoManager.addMultiple(videos, channel.id)
        })
}

const channelExports = {
    ...ChannelManager,
    add: addChannel,
    update: updateChannel,
}

export { channelExports as channel, VideoManager as video }
