import { Sequelize } from 'sequelize'
import { DataManager } from '..'
import { DBManager } from '../dbManager'
import { Video } from '../models'
import Channel from '../models/channel'
import * as YT from '../youtube'

async function add(channelId: string): Promise<Channel> {
    let channelInfo = await this.getChannelInfo(channelId)
    const newChannel = new Channel({
        channel_id: channelId,
        description: channelInfo.description,
        image_url: channelInfo.imageURL,
        name: channelInfo.name,
        all_videos_playlist_id: channelInfo.playlistId,
        last_fetched_page: null,
    })

    await newChannel.save()
    return newChannel
}

async function update(channelDBId: string): Promise<Channel> {
    let channels = await Channel.findAll({ where: { id: channelDBId } })
    if (!(channels.length > 0)) return
    let channel = channels[0]
    let channelInfo = await this.getChannelInfo(channel.channel_id)
    channel.image_url = channelInfo.imageURL
    channel.name = channelInfo.name
    channel.description = channelInfo.description
    channel.all_videos_playlist_id = channelInfo.playlistId
    return await channel.save()
}
async function remove(channelDBId: string) {
    await Channel.destroy({ where: { id: channelDBId } })
}

async function getTop(n: number = 10): Promise<Channel[]> {
    return this.getPage()
}
async function getPage(page: number = 0, n: number = 10): Promise<Channel[]> {
    let channels = await Channel.findAll({
        offset: page * n,
        limit: n,
    })

    let promises = channels.map(
        (c): Promise<Channel> =>
            new Promise(async (res, rej) => {
                let totalDownloaded = await Video.count({
                    where: {
                        channelId: c.id,
                        downloaded: true,
                    },
                })
                let totalPending = await Video.count({
                    where: {
                        channelId: c.id,
                        downloaded: false,
                    },
                })
                c.totalPending = totalPending
                c.totalDownloaded = totalDownloaded
                res(c)
            })
    )
    return Promise.all(promises)
}

async function getChannelInfo(channelId: string) {
    return YT.getChannelInfo(channelId)
}

export { add, update, remove, getTop, getPage, getChannelInfo }
