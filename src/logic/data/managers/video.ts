import { DBManager } from '../dbManager'
import { Channel, Video } from '../models'
import { VideoCreationAttributes } from '../models/video'
import * as YT from '../youtube'

async function addMultiple(videos: YT.VideoResponse[], channelDbId: string) {
    return Video.bulkCreate(videos.map((v) => transform(v, channelDbId)))
}

function transform(
    video: YT.VideoResponse,
    channelDbId: string
): VideoCreationAttributes {
    return {
        title: video.title,
        description: video.description,
        image_url: video.imageURL,
        video_id: video.id,
        channelId: channelDbId,
    }
}

async function getChannelVideos(
    channel: Channel
): Promise<YT.ChannelVideosResponse> {
    return YT.getChannelVideos(
        channel.all_videos_playlist_id,
        channel.last_fetched_page,
        (progress) => {}
    )
}

export { addMultiple, getChannelVideos }
