import Automator from '../automator'
import { DBManager } from './dbManager'
import { Channel, Video } from './models'

export interface MySQLCreds {
    host: string
    port: string
    dbName: string
    user: string
    pass: string
}

export interface MySQLCredsEnv {
    DB_HOST: string
    DB_PORT: string
    DB_NAME: string
    DB_USER: string
    DB_PASS: string
}

export class DataManager {
    private static instance: DataManager

    dbManager: DBManager

    private automator: Automator
    private secureKey: string
    private constructor() {}
    public static getInstance(): DataManager {
        if (!DataManager.instance) {
            DataManager.instance = new DataManager()
        }

        return DataManager.instance
    }

    public async loadFromEnv() {
        let creds = this.getMySQLCreds()
        await this.prepareDBManager()
    }

    public async setSecureKey(key: string) {
        this.secureKey = key
        await this.prepareDBManager()
    }

    public isUnlocked() {
        return this.dbManager ? true : false
    }

    private getMySQLCreds(): MySQLCreds {
        const { DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS } = process.env
        return {
            dbName: DB_NAME,
            host: DB_HOST,
            pass: DB_PASS,
            port: DB_PORT,
            user: DB_USER,
        }
    }

    private async prepareDBManager() {
        let creds = await this.getMySQLCreds()
        if (this.dbManager) {
            this.dbManager.closeConnection()
        }
        this.dbManager = new DBManager(creds)
        await this.dbManager.connect()

        this.dbManager.sequelize.addModels([Channel, Video])

        await this.dbManager.sequelize.sync()

        this.automator = new Automator()
    }
}
