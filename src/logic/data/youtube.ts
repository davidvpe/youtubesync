import axios from 'axios'

const base = 'https://youtube.googleapis.com/youtube/v3'

interface ChannelInfoResponse {
    name: string
    description: string
    imageURL: string
    playlistId: string
}

export interface ChannelVideosResponse {
    videos: VideoResponse[]
    lastPage?: string
}

export interface VideoResponse {
    id: string
    title: string
    description: string
    imageURL: string
}

const apiKey = () => {
    return process.env.YT_KEY
}

export const getChannelInfo = async (
    channelId: string
): Promise<ChannelInfoResponse> => {
    const url = `${base}/channels?part=snippet%2CcontentDetails&id=${channelId}&key=${apiKey()}`
    return axios
        .get(url)
        .then((res) => res.data)
        .then((res) => res.items[0])
        .then((res) => ({
            name: res.snippet.title,
            description: res.snippet.description,
            imageURL: res.snippet.thumbnails.default.url,
            playlistId: res.contentDetails.relatedPlaylists.uploads,
        }))
}

export const getChannelVideos = async (
    playlistId: string,
    nextPageToken: string = null,
    progressCallback: (progress: number) => void
): Promise<ChannelVideosResponse> => {
    const topVideos = 50
    let videos: VideoResponse[] = []
    let lastPage = nextPageToken
    let totalFromAPI = 0
    do {
        const url = `${base}/playlistItems?part=snippet%2CcontentDetails&playlistId=${playlistId}&maxResults=${topVideos}${
            nextPageToken ? `&pageToken=${nextPageToken}` : ''
        }&key=${apiKey()}`

        lastPage = nextPageToken

        let videosFromAPI = await axios
            .get(url)
            .then((res) => res.data)
            .then((res) => {
                totalFromAPI = res.pageInfo.totalResults
                nextPageToken = res.nextPageToken || null
                return res.items
            })
            .then((res: any[]): VideoResponse[] => {
                return res.map((video) => ({
                    id: video.contentDetails.videoId,
                    title: video.snippet.title,
                    description: video.snippet.description,
                    imageURL: video.snippet.thumbnails.default.url,
                }))
            })
        videos.push(...videosFromAPI)
        progressCallback((totalFromAPI / videos.length) * 100)
    } while (nextPageToken)

    return { videos: videos, lastPage: lastPage }
}
