import { MySQLCreds } from '.'

import { Sequelize } from 'sequelize-typescript'

export class DBManager {
    isValid: boolean | null = null
    error: string
    sequelize: Sequelize
    public constructor(public creds: MySQLCreds) {
        console.log('Building up db manager')
        this.sequelize = new Sequelize({
            host: creds.host,
            database: creds.dbName,
            port: Number(creds.port),
            username: creds.user,
            dialect: 'mysql',
            password: creds.pass,
        })
    }

    public async connect() {
        try {
            await this.sequelize.authenticate()
            console.log('Authentication possible')
            this.isValid = true
        } catch (error) {
            console.log('Authentication failed, ', error)
            this.error = error
            this.isValid = false
        }
    }

    public closeConnection() {
        this.sequelize.close()
    }
}
