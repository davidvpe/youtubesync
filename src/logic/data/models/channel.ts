import { Optional } from 'sequelize'
import { Table, Model, Column, HasMany } from 'sequelize-typescript'
import Video from './video'

interface ChannelAttributes {
    id: number
    channel_id: string
    name: string
    description: string
    image_url: string
    all_videos_playlist_id: string
    last_fetched_page: string
}

interface ChannelCreationAttributes
    extends Optional<ChannelAttributes, 'id' | 'last_fetched_page'> {}

@Table
class Channel extends Model<ChannelAttributes, ChannelCreationAttributes> {
    @Column
    channel_id: string
    @Column
    name: string
    @Column
    description: string
    @Column
    image_url: string
    @Column
    all_videos_playlist_id: string
    @Column
    last_fetched_page: string

    @HasMany(() => Video)
    videos: Video[]

    totalDownloaded?: number
    totalPending?: number
}

export default Channel
