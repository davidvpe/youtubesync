import { Optional } from 'sequelize'
import {
    Table,
    Model,
    Column,
    BelongsTo,
    ForeignKey,
    Default,
} from 'sequelize-typescript'

import Channel from './channel'

interface VideoAttributes {
    id: number
    video_id: string
    title: string
    description: string
    image_url: string
    channelId: string
    downloaded: boolean
    error: string
}

export interface VideoCreationAttributes
    extends Optional<VideoAttributes, 'id' | 'downloaded' | 'error'> {}

@Table
class Video extends Model<VideoAttributes, VideoCreationAttributes> {
    @Column
    video_id: string
    @Column
    title: string
    @Column
    description: string
    @Column
    image_url: string
    @Column
    error: string

    @Default(false)
    @Column
    downloaded: boolean

    @ForeignKey(() => Channel)
    @Column
    channelId: number

    @BelongsTo(() => Channel)
    channel: Channel
}

export default Video
