import { getTop } from '../data/managers/channel'
import { Channel, Video } from '../data/models'
import * as HomeSockets from '../sockets/home'

const youtubedl = require('youtube-dl-exec')

class Automator {
    channels: Channel[] = []
    queue: Video[] = []

    constructor() {
        this.start()
    }

    private async start() {
        this.channels = await Channel.findAll()
        this.pickChannel()
    }

    private pickChannel() {
        let channel = this.channels.pop()
        if (channel) {
            this.processChannel(channel)
        } else {
            this.start()
        }
    }

    private async processChannel(channel: Channel) {
        let pending = await Video.findAll({
            where: {
                downloaded: false,
                channelId: channel.id,
                error: null,
            },
            limit: 5,
        })
        if (pending.length === 0) {
            await new Promise((r) => {
                setTimeout(r, 60 * 1000)
            })
            this.pickChannel()
        } else {
            this.queue.push(...pending)
            this.processQueue(channel)
        }
    }

    private async processQueue(channel: Channel) {
        while (this.queue.length > 0) {
            let videoToDownload = this.queue.pop()

            let dir =
                process.env.NODE_ENV === 'production' ? '/videos' : './videos'

            try {
                let res = await youtubedl(
                    `https://www.youtube.com/watch?v=${videoToDownload.video_id}`,
                    {
                        output: `${dir}/${channel.name}/%(title)s.%(ext)s`,
                    }
                )
                videoToDownload.downloaded = true
                videoToDownload.save()
                HomeSockets.reportDownloadUpdate(videoToDownload.channelId)
                console.log(
                    'Successfully downloaded ',
                    videoToDownload.video_id
                )
            } catch (error) {
                const foundError = error as Error
                videoToDownload.error = foundError.message
                videoToDownload.save()
                console.log('Error with video ', videoToDownload.video_id)
            }
        }

        console.log('Empty queue, moving on...')

        this.processChannel(channel)
    }
}
export default Automator
