import * as fs from 'fs'
import { DataManager } from './data'

interface ExtraOptions {
    errors: ErrorViewModel[]
    [key: string]: any
}
export interface ErrorViewModel {
    short: string
    long?: string
}

export const defaultRenderOptions = async (
    extra: ExtraOptions = { errors: [] }
) => {
    //Validating...
    let errorsFound: ErrorViewModel[] = []

    let isUnlocked = await DataManager.getInstance().isUnlocked()
    if (!isUnlocked) {
        errorsFound.push({ short: 'Secure key not found. Fix it in Settings' })
    } else {
        let isDBConnected = DataManager.getInstance().dbManager.isValid
        if (!isDBConnected) {
            errorsFound.push({
                short: 'There is was a problem connecting to the DB',
                long: DataManager.getInstance().dbManager.error,
            })
        }
    }

    if (!extra.errors) {
        extra.errors = errorsFound
    } else {
        extra.errors = [...extra.errors, ...errorsFound]
    }

    return extra
}
