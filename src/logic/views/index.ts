import { MySQLCreds } from '../data/index'
import Channel from '../data/models/channel'
import { ErrorViewModel } from '../settings'

interface InputViewModel {
    id: string
    description: string
    secure: boolean
    value: string
    help: string
}

interface ChannelAddViewModel {
    errors: ErrorViewModel[]
    inputs: InputViewModel[]
}

interface HomeViewModel {
    errors: ErrorViewModel[]
    channels: Channel[]
}

export const viewModelForChannelAdd = (): ChannelAddViewModel => {
    return {
        errors: [],
        inputs: [
            {
                id: 'channel_id',
                description: 'Channel ID',
                help:
                    'This is the channel id... https://www.youtube.com/channel/<b>UCBVjMGOIkavEAhyqpxJ73Dw</b>  ',
                secure: false,
                value: '',
            },
        ],
    }
}

export const viewModelForHome = (channels: Channel[]): HomeViewModel => {
    return {
        errors: [],
        channels: channels,
    }
}
