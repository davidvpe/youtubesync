# Youtube Sync

Hi, this app will help you to keep all the videos for the channels you follow downloaded on your disk, and if left running it will automatically detect if a new video is released and download that video. this way all videos will be stored on a specific location protecting them from deletion or banning.
