FROM node:15-alpine

RUN apk add --no-cache \
    autoconf \
    automake \
    bash \
    g++ \
    libc6-compat \
    libjpeg-turbo-dev \
    libpng-dev \
    make \
    nasm \
    python

WORKDIR /pre

COPY package*.json ./

RUN npm install

COPY . ./

RUN npm run build

WORKDIR /app

RUN mv /pre/dist/* .

RUN rm -rf /pre

VOLUME [ "/app/data" ]

EXPOSE 3000

# CMD ["npm", "start"]
